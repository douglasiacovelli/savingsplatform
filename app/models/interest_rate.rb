class InterestRate < ApplicationRecord
  scope :country, -> (country) { where country: country }
  scope :today, -> { where date: Date.today }
end
