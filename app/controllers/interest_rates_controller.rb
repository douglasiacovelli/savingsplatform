class InterestRatesController < ApplicationController

  def index
    interest_rate = InterestRate.where(nil)
    interest_rate = interest_rate.country(interest_params[:country]) if interest_params[:country].present?
    interest_rate = interest_rate.today

    if interest_rate.present? || params[:country] != 'brazil'
      render json: interest_rate
    else
      interest_rates = fetch_interest_rate
      render json: interest_rates
    end
  end

    private
    def fetch_interest_rate
      require 'open-uri'
      require 'json'
      puts 'Fetching new data for Brazil'
      url = 'http://api.bcb.gov.br/dados/serie/bcdata.sgs.1178/dados?formato=json'
      json = JSON.parse(open(url).read)
      interest_rates = []
      last_interest = json.last
      interest_rates.push(InterestRate.create(name: 'selic', country: 'brazil', date: Date.today, interest: (last_interest["valor"].to_f)*100))
      interest_rates.push(InterestRate.create(name: 'poupança', country: 'brazil', date: Date.today, interest: (last_interest["valor"].to_f)*0.7*100))
    end

    def interest_params
      params.permit(:country)
    end

end
