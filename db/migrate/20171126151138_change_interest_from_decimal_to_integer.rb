class ChangeInterestFromDecimalToInteger < ActiveRecord::Migration[5.1]
  def change
    change_column :interest_rates, :interest, :integer
  end
end
