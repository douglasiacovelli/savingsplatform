class ChangeTypeFromInterestRates < ActiveRecord::Migration[5.1]
  def change
    rename_column :interest_rates, :type, :name
  end
end
