class CreateInterestRates < ActiveRecord::Migration[5.1]
  def change
    create_table :interest_rates do |t|
      t.date :date
      t.decimal :interest
      t.string :country
    end
  end
end
